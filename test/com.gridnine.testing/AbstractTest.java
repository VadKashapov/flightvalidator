package com.gridnine.testing;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AbstractTest {
    protected final Checker flightValidator;
    protected final List<Flight> flights = new ArrayList<>();

    protected final static LocalDateTime nowTime = LocalDateTime.now();
    protected final static Segment segment1 = new Segment(nowTime.plusHours(2), nowTime.plusHours(4));
    protected final static Segment segment2 = new Segment(nowTime.plusHours(5), nowTime.plusHours(8));
    protected final static Segment segment3 = new Segment(nowTime.minusHours(2), nowTime.plusHours(1));
    protected final static Segment segment4 = new Segment(nowTime.plusHours(4), nowTime.plusHours(3));
    protected final static Segment segment5 = new Segment(nowTime.plusHours(11),nowTime.plusHours(12));

    protected final static Flight flight1 = new Flight(Arrays.asList(segment1,segment2));
    protected final static Flight flight2 = new Flight(Collections.singletonList(segment3));
    protected final static Flight flight3 = new Flight(Collections.singletonList(segment4));
    protected final static Flight flight4 = new Flight(Arrays.asList(segment1,segment2,segment5));

    public AbstractTest(Checker checker) {
        this.flightValidator = checker;
    }
}
