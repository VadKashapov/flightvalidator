package com.gridnine.testing;

import java.util.ArrayList;
import java.util.List;

public class DepartureBeforeArriveChecker implements Checker {
    @Override
    public List<Flight> validate(List<Flight> flights) {
        List<Flight> list = new ArrayList<>();
        for (Flight flight : flights) {
            boolean fault = false;
            for (Segment segment : flight.getSegments()) {
                    if (segment.getArrivalDate().isBefore(segment.getDepartureDate())) {
                        fault = true;
                        break;
                    }
            }
            if (!fault) {
                list.add(flight);
            }
        }
        return list;
    }
}
