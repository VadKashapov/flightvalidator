package com.gridnine.testing;

import java.util.List;

interface Checker {
    List<Flight> validate(List<Flight> flights);
}
