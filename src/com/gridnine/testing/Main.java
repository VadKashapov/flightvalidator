package com.gridnine.testing;

import java.util.List;

public class Main {



    public static void main(String[] args) {
        List<Flight> flights = FlightBuilder.createFlights();
        printFlights(flights,"");

        FlightValidator flightValidator = new FlightValidator(flights);
        DepartureChecker departureChecker = new DepartureChecker();
        flightValidator.setFlightChecker(departureChecker);
        printFlights(flightValidator.validateFlights(), "Вылет до текущего момента времени");

        Checker arriveBeforeDeparture = new DepartureBeforeArriveChecker();
        flightValidator.setFlightChecker(arriveBeforeDeparture);
        printFlights(flightValidator.validateFlights(), "Имеются сегметны с датой прилета раньше даты вылета");

        Checker  groundTimeIsGreaterThan2Hrs = new GroundTimeIsGreaterTwoHrsChecker();
        flightValidator.setFlightChecker(groundTimeIsGreaterThan2Hrs);
        printFlights(flightValidator.validateFlights(), "Общее время, проведённое на земле превышает два часа");
    }

    public static void printFlights(List<Flight> flights, String description) {
        System.out.println(description);
        System.out.println("________________");
        for (Flight flight : flights) {
            System.out.println(flight);
        }
    }

}

