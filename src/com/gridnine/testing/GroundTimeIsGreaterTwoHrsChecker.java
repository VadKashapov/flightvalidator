package com.gridnine.testing;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class GroundTimeIsGreaterTwoHrsChecker implements Checker {
    @Override
    public List<Flight> validate(List<Flight> flights) {
        List<Flight> list = new ArrayList<>();
        for (Flight flight : flights) {
            boolean fault = false;
            for (int i = 1; i < flight.getSegments().size(); i++) {
                if (ChronoUnit.HOURS.between(flight.getSegments().get(i-1).getArrivalDate()
                        , flight.getSegments().get(i).getDepartureDate()) > 2) {
                    fault = true;
                    break;
                }
            }
            if (!fault) {
                list.add(flight);
            }
        }
        return list;
    }
}
