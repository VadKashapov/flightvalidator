package com.gridnine.testing;

import java.util.List;

public class FlightValidator {
    private List<Flight> flights;
    private Checker flightChecker;

    public FlightValidator(List<Flight> flights) {
        this.flights = flights;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public Checker getFlightChecker() {
        return flightChecker;
    }

    public void setFlightChecker(Checker flightChecker) {
        this.flightChecker = flightChecker;
    }

    public List<Flight> validateFlights() {
        return flightChecker.validate(flights);
    }
}
