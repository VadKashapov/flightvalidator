package com.gridnine.testing;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DepartureChecker implements Checker {
    @Override
    public List<Flight> validate(List<Flight> flights) {
        List<Flight> list = new ArrayList<>();
        for (Flight flight : flights) {
            boolean fault = false;
            for (Segment segment : flight.getSegments()) {
                if (LocalDateTime.now().isAfter(segment.getDepartureDate())) {
                    fault = true;
                    break;
                }
            }
            if (!fault) {
                list.add(flight);
            }
        }
        return list;
    }
}
